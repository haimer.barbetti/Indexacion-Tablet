package org.cgiar.ciat.indexacion_tablet;

import java.util.ArrayList;

import org.cgiar.ciat.indexacion_tablet.model.Accesion;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import android.view.View.OnClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;


public class FragmentActividadPCRRecepcion extends Fragment {
	
	public FragmentActividadPCRRecepcion(){
		
	}
	
//public int status;	
   @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
      
	   
	   View view=inflater.inflate(R.layout.fragment_pcr_actividad_recepcion, container, false);
	   
	   ListView lviewResultado=(ListView) view.findViewById(R.id.lview_resultado);
	   
	   //int status=MetodoEvaluacionPCRActivity.status;
	   
	   //if(status==0){
		   //lviewResultado.setAdapter(null);
	   //}else{
		   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
				
				@Override
				public void onInput(Object input, View view) {
					
					TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
					TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
					final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
					final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
					
					TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
					
					idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
					accesionTxt.setText(   ((Accesion) input).getNumero() );
					descripcionTxt.setText("");
					ublicacionTxt.setText("");
					
					descripcionTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							descripcionTxt.setEnabled(true);
							descripcionTxt.requestFocus();
							
						}
					});
					
					estadoTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View view) {
							int indexActual=0;
							ArrayList<String> listEstados=new ArrayList<String>();
							
							listEstados.add("Pendiente");
							listEstados.add("No Recibido");
							listEstados.add("Recibido");
						
							if(estadoTxt.getText().toString().equals("Pendiente")){
								estadoTxt.setText(listEstados.get(1).toString());
							}else if(estadoTxt.getText().toString().equals("No Recibido")){
								estadoTxt.setText(listEstados.get(2).toString());
							}else if(estadoTxt.getText().toString().equals("Recibido")){
								estadoTxt.setText(listEstados.get(0).toString());
							}else{
								estadoTxt.setText(listEstados.get(0).toString());
							}
							
							
						}
						
					});
					
				}
			   });*/
	   //}
	   
	   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
		
		@Override
		public void onInput(Object input, View view) {
			
			TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
			TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
			final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
			final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
			
			TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
			
			idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
			accesionTxt.setText(   ((Accesion) input).getNumero() );
			descripcionTxt.setText("");
			ublicacionTxt.setText("");
			
			descripcionTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					descripcionTxt.setEnabled(true);
					descripcionTxt.requestFocus();
					
				}
			});
			
			estadoTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					int indexActual=0;
					ArrayList<String> listEstados=new ArrayList<String>();
					
					listEstados.add("Pendiente");
					listEstados.add("No Recibido");
					listEstados.add("Recibido");
				
					if(estadoTxt.getText().toString().equals("Pendiente")){
						estadoTxt.setText(listEstados.get(1).toString());
					}else if(estadoTxt.getText().toString().equals("No Recibido")){
						estadoTxt.setText(listEstados.get(2).toString());
					}else if(estadoTxt.getText().toString().equals("Recibido")){
						estadoTxt.setText(listEstados.get(0).toString());
					}else{
						estadoTxt.setText(listEstados.get(0).toString());
					}
					
					
				}
				
			});
			
		}
	   });*/
	   
	   
      return view;
   }
   
   public void clearList(ListView lv){
	   lv.setAdapter(null);
   }
   
   
}
