package org.cgiar.ciat.indexacion_tablet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.cgiar.ciat.indexacion_tablet.model.Accesion;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;


public class FragmentActividadPCRSintesis extends Fragment {
	
	public FragmentActividadPCRSintesis(){
		
	}

   @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
      
	   
	   final View view=inflater.inflate(R.layout.fragment_pcr_actividad_sintesis, container, false);
	   
	   
	   ListView lviewResultado=(ListView) view.findViewById(R.id.lview_resultado);
	   
	   ImageButton byAcesionFilterBtn=(ImageButton) view.findViewById(R.id.find_by_accesion_btn);
	   
	   ImageButton activaFechaInternoBtn=(ImageButton) view.findViewById(R.id.activa_fecha_interno_btn);
	   ImageButton activaFechaGelesInternoBtn=(ImageButton) view.findViewById(R.id.activa_fecha_geles_interno_btn);
	   
	   final TextView fechaTxt=(TextView) view.findViewById(R.id.txt_fecha_edit);
	   final EditText editTextObservacion=(EditText) view.findViewById(R.id.txt_observacion_interno_edit);
	   
	   byAcesionFilterBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

				//System.out.println("Fecha: "+dateFormat.format(date));
				
				
				/*Log.d(" Imprime ","Prueba");*/
				FragmentManager manager=getFragmentManager();
				EditAccionFilterCustomDialogFragment dialogFragment=new EditAccionFilterCustomDialogFragment("Buscar Accesion");
				dialogFragment.show(manager, "");
				
				
				
			}
			
		});
	   
	   activaFechaInternoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Bundle arguments = new Bundle();
		        
				arguments.putInt("idFechaTxt",(Integer)R.id.txt_fecha_edit);
		        arguments.putInt("idObservacionEdit",(Integer)R.id.txt_observacion_interno_edit);
		        
		        arguments.putInt("idGelesInternoLy",(Integer)R.id.geles_pcr_interno);
		        arguments.putInt("idFechaTxtGelesInterno",(Integer)R.id.txt_fecha_geles_interno);
		        arguments.putInt("idEstadoTxtGelesInternoSpinner",(Integer)R.id.sp_estado_geles_interno);
		        arguments.putInt("idObservacionTxtGelesInternoEdit",(Integer)R.id.txt_observacion_geles_interno_edit);
		        
		        arguments.putString("titulo", "Fecha PCR Control Interno");
		        arguments.putString("validador", "principal");//boton principal
		        
		        
		        FragmentManager manager=getFragmentManager();
				EditCalendarioCustomDialogFragment dialogFragment=new EditCalendarioCustomDialogFragment();
		        
				dialogFragment.setArguments(arguments);
		        dialogFragment.show(manager, "");
				
				
			}
			
		});
	   
	   activaFechaGelesInternoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Bundle arguments = new Bundle();
		        
				arguments.putInt("idEstadoTxtGelesInternoSpinner",(Integer)R.id.sp_estado_geles_interno);
		        arguments.putInt("idObservacionTxtGelesInternoEdit",(Integer)R.id.txt_observacion_geles_interno_edit);
		        arguments.putString("titulo", "Fecha Geles Control Interno");
		        arguments.putString("validador", "depende");//dependiente del boton principal
		        
		        
		        
		        FragmentManager manager=getFragmentManager();
				EditCalendarioCustomDialogFragment dialogFragment=new EditCalendarioCustomDialogFragment();
		        
				dialogFragment.setArguments(arguments);
		        dialogFragment.show(manager, "");
				
				
			}
			
		});
	   //int status=MetodoEvaluacionPCRActivity.status;
	   
	   //if(status==0){
		   //lviewResultado.setAdapter(null);
	   //}else{
		   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
				
				@Override
				public void onInput(Object input, View view) {
					
					TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
					TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
					final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
					final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
					
					TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
					
					idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
					accesionTxt.setText(   ((Accesion) input).getNumero() );
					descripcionTxt.setText("");
					ublicacionTxt.setText("");
					
					descripcionTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							descripcionTxt.setEnabled(true);
							descripcionTxt.requestFocus();
							
						}
					});
					
					estadoTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View view) {
							int indexActual=0;
							ArrayList<String> listEstados=new ArrayList<String>();
							
							listEstados.add("Pendiente");
							listEstados.add("No Recibido");
							listEstados.add("Recibido");
						
							if(estadoTxt.getText().toString().equals("Pendiente")){
								estadoTxt.setText(listEstados.get(1).toString());
							}else if(estadoTxt.getText().toString().equals("No Recibido")){
								estadoTxt.setText(listEstados.get(2).toString());
							}else if(estadoTxt.getText().toString().equals("Recibido")){
								estadoTxt.setText(listEstados.get(0).toString());
							}else{
								estadoTxt.setText(listEstados.get(0).toString());
							}
							
							
						}
						
					});
					
				}
			   });*/
	   //}
	   
	   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
		
		@Override
		public void onInput(Object input, View view) {
			
			TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
			TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
			final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
			final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
			
			TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
			
			idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
			accesionTxt.setText(   ((Accesion) input).getNumero() );
			descripcionTxt.setText("");
			ublicacionTxt.setText("");
			
			descripcionTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					descripcionTxt.setEnabled(true);
					descripcionTxt.requestFocus();
					
				}
			});
			
			estadoTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					int indexActual=0;
					ArrayList<String> listEstados=new ArrayList<String>();
					
					listEstados.add("Pendiente");
					listEstados.add("No Recibido");
					listEstados.add("Recibido");
				
					if(estadoTxt.getText().toString().equals("Pendiente")){
						estadoTxt.setText(listEstados.get(1).toString());
					}else if(estadoTxt.getText().toString().equals("No Recibido")){
						estadoTxt.setText(listEstados.get(2).toString());
					}else if(estadoTxt.getText().toString().equals("Recibido")){
						estadoTxt.setText(listEstados.get(0).toString());
					}else{
						estadoTxt.setText(listEstados.get(0).toString());
					}
					
					
				}
				
			});
			
		}
	   });*/
	   
	   
      return view;
   }
   
   public void clearList(ListView lv){
	   lv.setAdapter(null);
   }

  /* @Override
	public void onDialogPositiveClick(DialogFragment dialog) {
	
	   LayoutInflater inflaterSintesis = getActivity().getLayoutInflater();
	   LayoutInflater inflaterDialog=dialog.getActivity().getLayoutInflater();
	   
	   
	   View viewSintesis=inflaterSintesis.inflate(R.layout.fragment_pcr_actividad_sintesis,  null);
	   View viewDialog=inflaterDialog.inflate(R.layout.fragment_calendario_custom_dialog,  null);
	   
	   EditText editTextFecha=(EditText) viewSintesis.findViewById(R.id.txt_fecha_edit);
	   EditText editTextObservacion=(EditText) viewSintesis.findViewById(R.id.txt_observacion_interno_edit);
	   
	   String d="";
	   DatePicker fechaPcrInternoDatePicker =(DatePicker) viewDialog.findViewById(R.id.fecha_datepicker_pcr_interno);
	   d=fechaPcrInternoDatePicker.getDayOfMonth()+"/"+fechaPcrInternoDatePicker.getMonth()+"/"+fechaPcrInternoDatePicker.getYear();
	   editTextFecha.setText(d.toString());
	   editTextObservacion.setEnabled(true);
	   editTextObservacion.requestFocus();
}*/

   
   
}
