package org.cgiar.ciat.indexacion_tablet;

import java.util.ArrayList;

import org.cgiar.ciat.indexacion_tablet.model.Accesion;
import org.cgiar.ciat.indexacion_tablet.model.Actividad;
import org.cgiar.ciat.indexacion_tablet.model.Contrato;
import org.cgiar.ciat.indexacion_tablet.model.Estado;
import org.cgiar.ciat.indexacion_tablet.model.Evaluacion;
import org.cgiar.ciat.indexacion_tablet.model.MetodoPcr;
import org.cgiar.ciat.indexacion_tablet.model.Usuario;
import org.cgiar.ciat.indexacion_tablet.model.Virus;
import org.cgiar.ciat.indexacion_tablet.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
//import org.cgiar.ciat.test.model.Recepcion;


public class MainActivity extends ActionBarActivity {
	
private boolean userIsInteracting;

//public static ArrayList<Estado> listEstados;
public static ArrayList<Actividad> listActividades;
public static ArrayList<Virus> listVirus;
public static ArrayList<Usuario> list;
public static ArrayList<Contrato> listContrato;
public static ArrayList<Accesion> listAccesion;
public static ArrayList<Evaluacion> evaluacionesGeneral;
public static MetodoPcr metodoPcr;
Usuario userSelected;
Toast msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
         * EditText Elements
         */
        final EditText passTxt=(EditText) findViewById(R.id.input_txt_pass);
        
        /*
         * Button Elements
         */
        Button btnAceptar=(Button) findViewById(R.id.ingresar_btn);
        //Button btnSalir=(Button) findViewById(R.id.cancelar_btn);
        
        
        /*
         * Spinners Elements 
         */
        final Spinner spUsers=(Spinner) findViewById(R.id.sp_users);
        
        
        /*listEvaluaciones=new ArrayList<Evaluacion>();
        listEvaluaciones.add(new Evaluacion("ReoVirus", "PCR", "Negativo"));
        listEvaluaciones.add(new Evaluacion("ReoVirus", "GEL", "Negativo"));
        listEvaluaciones.add(new Evaluacion("LuteoVirus", "PCR", "Negativo"));
        listEvaluaciones.add(new Evaluacion("LuteoVirus", "GEL", "Negativo"));
        listEvaluaciones.add(new Evaluacion("CsNAV", "PCR", "Negativo"));
        listEvaluaciones.add(new Evaluacion("CsNAV", "GEL", "Negativo"));
        listEvaluaciones.add(new Evaluacion("Torrado Like Virus", "PCR", "Negativo"));
        listEvaluaciones.add(new Evaluacion("Torrado Like Virus", "GEL", "Negativo"));*/
        
        
        /*listEstados=new ArrayList<Estado>();
        listEstados.add(new Estado("No Inicinado",false));
        listEstados.add(new Estado("Iniciado",false));
        listEstados.add(new Estado("Terminado",false));
        listEstados.add(new Estado("No Verificado",false));
        listEstados.add(new Estado("Repeticion",false));
        listEstados.add(new Estado("Solicitud Tejido",false));
        listEstados.add(new Estado("No Verificado",false));
        */
        
        
        
        listActividades=new ArrayList<Actividad>();
        listActividades.add(new Actividad("Seleccione Actividad",new Estado("No Iniciado","Iniciado",true)));
        listActividades.add(new Actividad("Recepcion",new Estado("No Iniciado","Iniciado",true)));
        listActividades.add(new Actividad("Extraccion RNA Total",new Estado("No Iniciado","Iniciado",true)));
        listActividades.add(new Actividad("Sintesis cRNA",new Estado("No Iniciado","Iniciado",true)));
        listActividades.add(new Actividad("Resultados",new Estado("No Iniciado","Iniciado",true)));
        //listActividades.add(new Actividad("RNA Total Obtenido",new Estado("No Iniciado","Iniciado",true)));
        //listActividades.add(new Actividad("Verificacion resultados",new Estado("No Verificado","Verificado",true)));
        
        listVirus=new ArrayList<Virus>();
        listVirus.add(new Virus("ReoVirus",false));
        listVirus.add(new Virus("LuteoVirus",false));
        listVirus.add(new Virus("CsNAV",false));
        listVirus.add(new Virus("Torrado Like Virus",false));
        listVirus.add(new Virus("Fitosanitario",false));
        
        
        list=new ArrayList<Usuario>();
        list.add(new Usuario("Seleccione Responsable", "", ""));
		list.add(new Usuario("Haimer Barbetti", "", "123456"));
        list.add(new Usuario("Operador 1", "", "123456"));
        list.add(new Usuario("Diego Fernando", "", "123456"));
        list.add(new Usuario("Operador 2", "", "123456"));
        list.add(new Usuario("Operador 3", "", "123456"));
        list.add(new Usuario("Marixa Cuervo", "", "123456"));
        list.add(new Usuario("Angela Marcela Hernandez", "", "123456"));
        list.add(new Usuario("Tester", "", "123456"));
        
        
        listAccesion=new ArrayList<Accesion>();
        
        listAccesion.add(new Accesion(1,"RUB XXX- 33",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(1,"RUB XXX- 33",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(1,"RUB XXX- 33",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(1,"RUB XXX- 33",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(1,"RUB XXX- 33",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));

        listAccesion.add(new Accesion(2,"RUB XXX- 32",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(2,"RUB XXX- 32",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(2,"RUB XXX- 32",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(2,"RUB XXX- 32",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(2,"RUB XXX- 32",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));

        listAccesion.add(new Accesion(3,"RUB XXX- 34",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(3,"RUB XXX- 34",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(3,"RUB XXX- 34",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(3,"RUB XXX- 34",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(3,"RUB XXX- 34",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));

        listAccesion.add(new Accesion(4,"RUB XXX- 1", listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(4,"RUB XXX- 1", listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(4,"RUB XXX- 1", listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(4,"RUB XXX- 1", listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(4,"RUB XXX- 1", listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));

        listAccesion.add(new Accesion(5,"RUB XXX- 35",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(5,"RUB XXX- 35",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(5,"RUB XXX- 35",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(5,"RUB XXX- 35",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(5,"RUB XXX- 35",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(6,"RUB XXX- 96",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(6,"RUB XXX- 96",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(6,"RUB XXX- 96",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(6,"RUB XXX- 96",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(6,"RUB XXX- 96",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(7,"RUB XXX- 40",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(7,"RUB XXX- 40",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(7,"RUB XXX- 40",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(7,"RUB XXX- 40",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(7,"RUB XXX- 40",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(8,"RUB XXX- 41",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(8,"RUB XXX- 41",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(8,"RUB XXX- 41",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(8,"RUB XXX- 41",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(8,"RUB XXX- 41",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(9,"RUB XXX- 42",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(9,"RUB XXX- 42",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(9,"RUB XXX- 42",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(9,"RUB XXX- 42",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(9,"RUB XXX- 42",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(10,"RUB XXX- 43",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(10,"RUB XXX- 43",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(10,"RUB XXX- 43",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(10,"RUB XXX- 43",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(10,"RUB XXX- 43",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(11,"RUB XXX- 44",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(11,"RUB XXX- 44",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(11,"RUB XXX- 44",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(11,"RUB XXX- 44",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(11,"RUB XXX- 44",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(12,"RUB XXX- 45",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(12,"RUB XXX- 45",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(12,"RUB XXX- 45",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(12,"RUB XXX- 45",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(12,"RUB XXX- 45",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(13,"RUB XXX- 105",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(13,"RUB XXX- 105",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(13,"RUB XXX- 105",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(13,"RUB XXX- 105",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(13,"RUB XXX- 105",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(14,"RUB XXX- 104",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(14,"RUB XXX- 104",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(14,"RUB XXX- 104",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(14,"RUB XXX- 104",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(14,"RUB XXX- 104",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(15,"RUB XXX- 103",listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(15,"RUB XXX- 103",listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(15,"RUB XXX- 103",listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(15,"RUB XXX- 103",listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(15,"RUB XXX- 103",listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listAccesion.add(new Accesion(16,"RUB XXX- 65", listActividades,((Virus) listVirus.get(0)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(16,"RUB XXX- 65", listActividades,((Virus) listVirus.get(1)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(16,"RUB XXX- 65", listActividades,((Virus) listVirus.get(2)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(16,"RUB XXX- 65", listActividades,((Virus) listVirus.get(3)).getNombre().toString(),"2016-11-05"));
        listAccesion.add(new Accesion(16,"RUB XXX- 65", listActividades,((Virus) listVirus.get(4)).getNombre().toString(),"2016-11-05"));
        
        listContrato=new ArrayList<Contrato>();
        listContrato.add(new Contrato("Selecciones Contrato",listAccesion,""));
        listContrato.add(new Contrato("xx0002145",listAccesion,"Operador 2"));
        listContrato.add(new Contrato("xx0612145",listAccesion,"Operador 3"));
        
        
        metodoPcr=new MetodoPcr(listContrato);
        
        Log.d("metodo ", metodoPcr.toString());
        
        /*
         * Start Event Listeners
         */
        btnAceptar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
			    String pass=userSelected.getContrasenia().toString();
			    
			    if(pass.equals(passTxt.getText().toString())){
			    	
			    	msg=Toast.makeText(getApplicationContext(),
	    	                  "Usuario auth! user: "+userSelected.getNombres().toString()+" "+userSelected.getPosition(), Toast.LENGTH_SHORT);
      			    msg.show();
      			    
      			    Intent inten = new Intent(getApplicationContext(),MenuActivity.class);
      			    inten.putExtra("userFromIntent", userSelected);
      			    startActivity(inten);
      			    
			    }else if(passTxt.getText().toString()==null||passTxt.getText().toString()==""){
			    	msg=Toast.makeText(getApplicationContext(),
	    	                  "Campo de contraseņa vacio!", Toast.LENGTH_SHORT);
			    	passTxt.requestFocus();
    			    msg.show();
			    }else if(!pass.equals(passTxt.getText().toString())){
			    	msg=Toast.makeText(getApplicationContext(),
	    	                  "La constraseņa Ingresada Es incorrecta!", Toast.LENGTH_SHORT);
			    	passTxt.requestFocus();
  			        msg.show();
			    }else{
			    	
			    }
			}
			
		});
        /*btnSalir.setOnClickListener(new OnClickListener() {
			
			public void onClick(View view) {
				finish();			    
			}
			
		});*/
        spUsers.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int posicion, long id) {
				/*int posArray=(Integer) parent.getItemAtPosition(posicion);
				userSelected=(Usuario)list.get(posArray);*/
				
				
				spUsers.setSelection(posicion);
				
				
				Log.d("Debug ", " posicion "+posicion);
				userSelected=(Usuario)parent.getItemAtPosition(posicion);
				
				
				userSelected.setPosition(posicion);
				Log.d("Debug ", " posicion " + userSelected.getPosition() );
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
        	
		});
        /*
         * End Click Event Listeners
         */
        
        
        
        /*
         *Start set Adapters 
         */
        spUsers.setAdapter(new UsersSpinnerAdapter(getApplicationContext(),R.layout.sp_dropdown_item_row_responsable, list) {

			@Override
			public void onInput(Object input, View view) {
				
				TextView userNameTxt = (TextView) view.findViewById(R.id.userNameDropDown);
				//TextView userPassTxt=(TextView) view.findViewById(R.id.userPassDropDown);
				userNameTxt.setText(((Usuario) input).getNombres());
				//userPassTxt.setText(((Usuario) input).getContrasenia());
			}
			
		});
        
        
        
        
    }

    
    
   
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        	
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
    
    
    

}
