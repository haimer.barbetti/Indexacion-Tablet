package org.cgiar.ciat.indexacion_tablet;

import java.util.ArrayList;

//import com.example.test.R;




import org.cgiar.ciat.indexacion_tablet.model.Accesion;
import org.cgiar.ciat.indexacion_tablet.model.Contrato;
import org.cgiar.ciat.indexacion_tablet.model.Usuario;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnHoverListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public abstract class ResultadosListViewByContratoAdapter extends BaseAdapter {
    Context context;
	//private ArrayList<?> recepcionsList;
	private Contrato contrato;
	private ArrayList<Accesion> accesionList;
	//private static LayoutInflater inflater = null;
	//private View viewP;
	int viewId;

	/*public ResultadosListViewAdapter(Context context,int viewId, ArrayList<?> recepcionsList) {
		this.context = context;
		this.recepcionsList = recepcionsList;
		this.viewId=viewId;
	}*/
	public ResultadosListViewByContratoAdapter(Context context,int viewId, Contrato contrato) {
		this.context = context;
		this.contrato=contrato;
		this.viewId=viewId;
		this.accesionList=new ArrayList<Accesion>();
		this.accesionList=(ArrayList<Accesion>) contrato.getAccesiones();
	}

	@Override
	public int getCount() {
		//return recepcionsList.size();
		return accesionList.size();
		//return 1;
	}

	@Override
	public Object getItem(int itemId) {
		return accesionList.get(itemId);
	}

	@Override
	public long getItemId(int posicion) {
		return posicion;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(viewId, null);
		}
		//onInput(recepcionsList.get(position), convertView);
		onInput(accesionList.get(position), convertView);
		return convertView;

	}
	
	
	
	public abstract void onInput(Object input,View view);
	
}
