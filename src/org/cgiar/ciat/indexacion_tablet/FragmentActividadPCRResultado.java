package org.cgiar.ciat.indexacion_tablet;

import java.util.ArrayList;

import org.cgiar.ciat.indexacion_tablet.model.Accesion;
import org.cgiar.ciat.indexacion_tablet.model.Evaluacion;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;


public class FragmentActividadPCRResultado extends Fragment {
	
	
public static String tempVirus="";
public static 	String tempTipo="";
public static String tempEstado;
public static  int optSelected=0;
ArrayList<Evaluacion> evaluaciones=new ArrayList<Evaluacion>();
	public FragmentActividadPCRResultado(){
		
	}

   @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
      
	
	
	   View view=inflater.inflate(R.layout.fragment_pcr_actividad_resultado2, container, false);
	   
	   //-RadioGroup  radiosBtnsTipos=(RadioGroup) view.findViewById(R.id.input_radioGroup_tipo);
	   //-RadioButton radioOptGelTipo = (RadioButton) radiosBtnsTipos.getChildAt(0);
	   //-RadioButton radioOptPcrTipo = (RadioButton) radiosBtnsTipos.getChildAt(1);
       
	   
	   
	   //-final ListView lviewEvaluaciones=(ListView) view.findViewById(R.id.lview_evaluaciones);
       
	   //-Spinner spVirus=(Spinner)view.findViewById(R.id.sp_virus);
	   //-Spinner spEstados=(Spinner)view.findViewById(R.id.sp_evaluacion_estados);
	   //-Button agregarBtn=(Button) view.findViewById(R.id.agregar_btn);
	   
	 //-ImageButton byAcesionFilterBtn=(ImageButton) view.findViewById(R.id.find_by_accesion_btn);
	   
	   //evaluaciones=new ArrayList<Evaluacion>();
	   
	 /*  byAcesionFilterBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Log.d(" Imprime ","Prueba");
				FragmentManager manager=getFragmentManager();
				EditAccionFilterCustomDialogFragment dialogFragment=new EditAccionFilterCustomDialogFragment();
				dialogFragment.show(manager, "");
			}
			
		});*/
	   
	   
	  /* spVirus.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View arg1, int posicion,long arg3) {
			
			final AdapterView<?> adapterView=parent;
			final int p=posicion;
			tempVirus=adapterView.getItemAtPosition(p).toString();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
			
		}
	});
	   
	   spEstados.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int posicion,long arg3) {
				
				final AdapterView<?> adapterView=parent;
				final int p=posicion;
				tempEstado=adapterView.getItemAtPosition(p).toString();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
				
			}
		});
	   
	   
	   radiosBtnsTipos.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
				
				if(checkId==R.id.input_radio_tipo_pcr){
					optSelected=1;
				}else if(checkId==R.id.input_radio_tipo_pcr_gel){
					optSelected=2;
				}else{
					optSelected=0;
				}
				
						switch (optSelected) {
						case 0:
							tempTipo="";
							break;
						case 1:
							tempTipo="PCR";
							break;
						case 2:
							tempTipo="GEL";
							break;
						default:
							tempTipo="";
							break;
						}
			}
		});
	   
	   
	   agregarBtn.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			evaluaciones=new ArrayList<Evaluacion>();
			evaluaciones.add(new Evaluacion(tempVirus.toString(),tempTipo.toString(),tempEstado.toString()));
			
			MainActivity.evaluacionesGeneral=evaluaciones;
			//lviewEvaluaciones.setAdapter(null);
			//addEvaluacion(new Evaluacion(tempVirus.toString(),tempTipo.toString(),tempEstado.toString()));
			evaluaciones.add(new Evaluacion(tempVirus.toString(),tempTipo.toString(),tempEstado.toString()));
			
			
			Log.d(" Array -> ", MainActivity.evaluacionesGeneral.toString());
			
			lviewEvaluaciones.setAdapter(new EvaluacionesListViewAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_evaluaciones, MainActivity.evaluacionesGeneral) {
				
				@Override
				public void onInput(Object input, View view) {
					
					TextView virusTxt = (TextView) view.findViewById(R.id.tbl_resultado_virus);
					TextView tipoTxt = (TextView) view.findViewById(R.id.tbl_resultado_tipo);
					TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado_eval);
					
					virusTxt.setText( String.valueOf( ((Evaluacion) input).getVirus().toString()) );
					tipoTxt.setText( String.valueOf( ((Evaluacion) input).getTipo().toString()) );
					estadoTxt.setText( String.valueOf( ((Evaluacion) input).getEstado().toString()) );
					
				}
				
			});
			
			
			
		}
	});*/
	   
	 
	   
	   
	   
	/*if(lviewEvaluaciones!=null){
		
		lviewEvaluaciones.setAdapter(new EvaluacionesListViewAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_evaluaciones, MainActivity.evaluaciones) {
			
			@Override
			public void onInput(Object input, View view) {
				
				TextView virusTxt = (TextView) view.findViewById(R.id.tbl_resultado_virus);
				TextView tipoTxt = (TextView) view.findViewById(R.id.tbl_resultado_tipo);
				TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado_eval);
				
				virusTxt.setText( String.valueOf( ((Evaluacion) input).getVirus().toString()) );
				tipoTxt.setText( String.valueOf( ((Evaluacion) input).getTipo().toString()) );
				estadoTxt.setText( String.valueOf( ((Evaluacion) input).getEstado().toString()) );
				
			}
		});	
		
		
	}*/
	   
	   
	   //lviewEvaluaciones.setAdapter(adapter);
	   
	   //int status=MetodoEvaluacionPCRActivity.status;
	   
	   //if(status==0){
		   //lviewResultado.setAdapter(null);
	   //}else{
		   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
				
				@Override
				public void onInput(Object input, View view) {
					
					TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
					TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
					final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
					final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
					
					TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
					
					idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
					accesionTxt.setText(   ((Accesion) input).getNumero() );
					descripcionTxt.setText("");
					ublicacionTxt.setText("");
					
					descripcionTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							descripcionTxt.setEnabled(true);
							descripcionTxt.requestFocus();
							
						}
					});
					
					estadoTxt.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View view) {
							int indexActual=0;
							ArrayList<String> listEstados=new ArrayList<String>();
							
							listEstados.add("Pendiente");
							listEstados.add("No Recibido");
							listEstados.add("Recibido");
						
							if(estadoTxt.getText().toString().equals("Pendiente")){
								estadoTxt.setText(listEstados.get(1).toString());
							}else if(estadoTxt.getText().toString().equals("No Recibido")){
								estadoTxt.setText(listEstados.get(2).toString());
							}else if(estadoTxt.getText().toString().equals("Recibido")){
								estadoTxt.setText(listEstados.get(0).toString());
							}else{
								estadoTxt.setText(listEstados.get(0).toString());
							}
							
							
						}
						
					});
					
				}
			   });*/
	   //}
	   
	   /*lviewResultado.setAdapter(new ResultadosListViewByContratoAdapter(getActivity().getApplicationContext(),R.layout.lv_item_row_resultados,MetodoEvaluacionPCRActivity.contratoSelected) {
		
		@Override
		public void onInput(Object input, View view) {
			
			TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
			TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
			final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
			final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
			
			TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
			
			idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
			accesionTxt.setText(   ((Accesion) input).getNumero() );
			descripcionTxt.setText("");
			ublicacionTxt.setText("");
			
			descripcionTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					descripcionTxt.setEnabled(true);
					descripcionTxt.requestFocus();
					
				}
			});
			
			estadoTxt.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					int indexActual=0;
					ArrayList<String> listEstados=new ArrayList<String>();
					
					listEstados.add("Pendiente");
					listEstados.add("No Recibido");
					listEstados.add("Recibido");
				
					if(estadoTxt.getText().toString().equals("Pendiente")){
						estadoTxt.setText(listEstados.get(1).toString());
					}else if(estadoTxt.getText().toString().equals("No Recibido")){
						estadoTxt.setText(listEstados.get(2).toString());
					}else if(estadoTxt.getText().toString().equals("Recibido")){
						estadoTxt.setText(listEstados.get(0).toString());
					}else{
						estadoTxt.setText(listEstados.get(0).toString());
					}
					
					
				}
				
			});
			
		}
	   });*/
	   
	   
      return view;
   }
   
   public void clearList(ListView lv){
	   lv.setAdapter(null);
   }
   
   
   /*public void addEvaluacion(Evaluacion e){
	   this.evaluaciones.add(e);
   }*/
   
}
