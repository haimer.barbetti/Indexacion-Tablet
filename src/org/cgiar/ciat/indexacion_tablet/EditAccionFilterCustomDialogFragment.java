package org.cgiar.ciat.indexacion_tablet;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;


public class EditAccionFilterCustomDialogFragment extends DialogFragment {
	private String titulo;
	
	public EditAccionFilterCustomDialogFragment() {
		
	}

	
    public EditAccionFilterCustomDialogFragment(String titulo) {
		this.titulo = titulo;
	}



	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        
        builder	.setTitle(this.titulo)
        		.setView(inflater.inflate(R.layout.fragment_custom_dialog, null))
		        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
		                   
		        		   public void onClick(DialogInterface dialog, int id) {
		                       
		                   }
		        		   
		               })
		               
		               .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                    
		                   }
               });
        
        
        return builder.create();
    }


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	
	
}