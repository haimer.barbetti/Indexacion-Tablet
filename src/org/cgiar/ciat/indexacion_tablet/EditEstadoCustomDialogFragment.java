package org.cgiar.ciat.indexacion_tablet;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


public class EditEstadoCustomDialogFragment extends DialogFragment {
	ArrayList<Integer> mSelectedItems;
	private String titulo;
	
	public EditEstadoCustomDialogFragment() {
		
	}

	
    public EditEstadoCustomDialogFragment(String titulo) {
		this.titulo = titulo;
	}
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	mSelectedItems = new ArrayList<Integer>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        //builder.setMessage(" MENSAJE")
        
        builder.setTitle(this.titulo)
        //builder.setTitle("Estado Recepción")
        		.setItems(R.array.estados_arreglo, new DialogInterface.OnClickListener() {
        			public void onClick(DialogInterface dialog, int which) {
               
               		}
        		})
        		/*.setMultiChoiceItems(R.array.estados_arreglo, null, new DialogInterface.OnMultiChoiceClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which,boolean isChecked) {
					
					
					if (isChecked) {
	                       // If the user checked the item, add it to the selected items
	                       mSelectedItems.add(which);
	                   } else if (mSelectedItems.contains(which)) {
	                       // Else, if the item is already in the array, remove it
	                       mSelectedItems.remove(Integer.valueOf(which));
	                   }
					
				}
               })*/
        		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {//R.string.cancel
                   
        		   public void onClick(DialogInterface dialog, int id) {
                       
                   }
        		   
               })
               
               .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                    
                   }
               });
        
        return builder.create();
    }
    
    public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
    
}