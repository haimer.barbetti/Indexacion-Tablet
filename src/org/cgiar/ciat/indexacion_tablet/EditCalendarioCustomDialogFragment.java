package org.cgiar.ciat.indexacion_tablet;


import java.util.ArrayList;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


public class EditCalendarioCustomDialogFragment extends DialogFragment  {
	
	public static final String TAG = "ExampleFragment";
	ArrayList<Integer> mSelectedItems;
	private String titulo;
	private int idFechaTxt;
	private int idObservacionEdit;
	private int idGelesInternoLy;
	
	private int idFechaTxtGelesInterno;
	private int idEstadoTxtGelesInternoSpinner;
	private int idObservacionTxtGelesInternoEdit;
	private String validador;
	
	public EditCalendarioCustomDialogFragment() {
		
	}

	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
    	
    	final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();  
        final View view=inflater.inflate(R.layout.fragment_calendario_custom_dialog, null);
        
        this.idFechaTxt=getArguments()!=null?getArguments().getInt("idFechaTxt"):0;
        this.idObservacionEdit=getArguments()!=null?getArguments().getInt("idObservacionEdit"):0;
        this.idGelesInternoLy=getArguments()!=null?getArguments().getInt("idGelesInternoLy"):0;
        this.titulo=getArguments()!=null?getArguments().getString("titulo"):"";
        
        this.idFechaTxtGelesInterno=getArguments()!=null?getArguments().getInt("idFechaTxtGelesInterno"):0;
        this.idEstadoTxtGelesInternoSpinner=getArguments()!=null?getArguments().getInt("idEstadoTxtGelesInternoSpinner"):0;
        this.idObservacionTxtGelesInternoEdit=getArguments()!=null?getArguments().getInt("idObservacionTxtGelesInternoEdit"):0;
        this.validador=getArguments()!=null?getArguments().getString("validador"):"";
        
        
        builder.setTitle(this.titulo)
        	   .setView(inflater.inflate(R.layout.fragment_calendario_custom_dialog, null))
        	   .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
        		   
                   public void onClick(DialogInterface dialog, int id) {
                	   
        			   String d="";
        			   
        			   DatePicker fechaPcrInternoDatePicker =(DatePicker) view.findViewById(R.id.fecha_datepicker_pcr_interno);
        			   
        			   d=fechaPcrInternoDatePicker.getDayOfMonth()+"/"+fechaPcrInternoDatePicker.getMonth()+"/"+fechaPcrInternoDatePicker.getYear();
        			   
        			   TextView fechaTxt=(TextView) getActivity().findViewById(idFechaTxt);
        			   EditText observacionEdit=(EditText) getActivity().findViewById(idObservacionEdit);
        			   
        			   
        			   TextView fechaGelesInternoTxt=(TextView) getActivity().findViewById(idFechaTxtGelesInterno);
        			   Spinner estadoGelesInternoSpinner=(Spinner) getActivity().findViewById(idEstadoTxtGelesInternoSpinner);
        			   EditText observaciongelesInternoEdit=(EditText) getActivity().findViewById(idObservacionTxtGelesInternoEdit);
        			   LinearLayout gelesInternoLy=(LinearLayout)  getActivity().findViewById(idGelesInternoLy);
        			   
        			   
        			   
        			   if(validador.equals("principal")){
        				   fechaTxt.setText(d.toString());
            			   observacionEdit.setEnabled(true);
            			   observacionEdit.requestFocus();
            			   gelesInternoLy.setVisibility(View.VISIBLE);
            			   //estadoGelesInternoSpinner.setClickable(true);
            			   //observaciongelesInternoEdit.setEnabled(true);
        			   }else if(validador.equals("depende")){
        				   //fechaTxt.setText(d.toString());
            			   //observacionEdit.setEnabled(true);
            			   //observacionEdit.requestFocus();
            			   
            			   //gelesInternoLy.setVisibility(View.VISIBLE);
            			   estadoGelesInternoSpinner.setClickable(true);
            			   observaciongelesInternoEdit.setEnabled(true);
            			   observaciongelesInternoEdit.requestFocus();
        			   }else{
        				   
        			   }
        			   
        			  /* fechaTxt.setText(d.toString());
        			   observacionEdit.setEnabled(true);
        			   observacionEdit.requestFocus();
        			   gelesInternoLy.setVisibility(View.VISIBLE);
        			   
        			    estadoGelesInternoSpinner.setClickable(true);
        			    observaciongelesInternoEdit.setEnabled(true);*/
        			  
                	}
        		   
               })
               .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                    
                   }
               });
        
        return builder.create();
    }
    
    public void onDateSet(DatePicker view, int year, int month, int day) {
        
    }
    
    
    public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	
	
}
