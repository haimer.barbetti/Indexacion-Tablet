package org.cgiar.ciat.indexacion_tablet;


import java.util.ArrayList;

import org.cgiar.ciat.indexacion_tablet.R;
import org.cgiar.ciat.indexacion_tablet.model.Usuario;



import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;





import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;
import android.os.Build;

public class MenuActivity extends Activity {
	
	int optSelected;
	Toast msg;
	public Usuario userFromIntent;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		optSelected=0;
		//userFromIntent=new Usuario();
		userFromIntent=(Usuario) getIntent().getSerializableExtra("userFromIntent");
		
		/*
         * EditText Elements
         */
        
        
        /*
         * Button Elements
         */
        Button btnAceptar=(Button) findViewById(R.id.aceptar_btn);
        Button btnSalir=(Button) findViewById(R.id.salir_btn);
        
        
        /*
         * Spinners Elements 
         */
        
        
        /*
         * Radio Elements
         */
        final RadioGroup  radiosBtnsMenu=(RadioGroup) findViewById(R.id.input_radioGroup_menu);
        final RadioButton radioOptElisa = (RadioButton) radiosBtnsMenu.getChildAt(0);
        final RadioButton radioOptPcr = (RadioButton) radiosBtnsMenu.getChildAt(1);
        final RadioButton radioOptreportes = (RadioButton) radiosBtnsMenu.getChildAt(2);
        
        /*
         * Start Event Listeners
         */
		btnAceptar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				
				Intent inten=null;
				if(optSelected==0){
					msg=Toast.makeText(getApplicationContext(),
		  	                  "Seleccione una Opcion en el Menu! ", Toast.LENGTH_SHORT);
					msg.show();
				}else if(optSelected==1){
					msg=Toast.makeText(getApplicationContext(),
		  	                  "Accede a Menu Metodo Elisa! ", Toast.LENGTH_SHORT);
					msg.show();
				}else if(optSelected==2){
					
					inten = new Intent(getApplicationContext(),MetodoEvaluacionPCRActivity.class);
					msg=Toast.makeText(getApplicationContext(),"Accede a Menu RT-PCR! ", Toast.LENGTH_SHORT);
					
					inten.putExtra("userActive", userFromIntent);
	      			startActivity(inten);
					
	      			
	      		   /*Fragment fragment=new FragmentActividadPCRRecepcion();
	      		   FragmentManager manager=getFragmentManager();
	      		   FragmentTransaction transaction=manager.beginTransaction();
	      		   transaction.replace(R.id.fragment_container, fragment);
	      		   transaction.commit();*/
	      			
	      			
	      			
					msg.show();
					
				}else if(optSelected==3){
					
				}else{
					
				}
				
			    
				//Intent inten = new Intent(getApplicationContext(),MenuPcrActivity.class);
  			    
					
			}

		});
		btnSalir.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				Intent inten = new Intent(getApplicationContext(),MainActivity.class);
  			    startActivity(inten);
				finish();
			}

		});
		radiosBtnsMenu.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
				if(checkId==R.id.input_radio_metodo_elisa){
					optSelected=1;
				}else if(checkId==R.id.input_radio_metodo_rtpcr){
					optSelected=2;
				}else if(checkId==R.id.input_radio_menu_reportes){
					optSelected=3;
				}
			}
		});
		
		/*if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_menu, container,
					false);
			return rootView;
		}
	}
	
	@Override
    public void onBackPressed() {
        return;
    }

}
