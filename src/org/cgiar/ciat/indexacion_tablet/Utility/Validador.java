package org.cgiar.ciat.indexacion_tablet.Utility;

import android.text.TextUtils;

public class Validador {
	static String r="";

	public static final String formatAccesion(String accesion) {
        accesion = accesion.replaceAll("[^a-zA-Z0-9]+","");
        accesion = accesion.trim().toUpperCase();
     
              if (!TextUtils.isEmpty(accesion)){
                     if (accesion.charAt(0) == 'G') {
                            accesion = accesion.substring(1);
                            String accesionLongitud = "";
                            char caracter;
                            int i;
                            for (i = 0; i < accesion.length(); i++) {
                                   caracter = accesion.charAt(i);
                                   if (caracter != ' ' && !Character.isLetterOrDigit(caracter)) {
                                         break;
                                   }
                                   if (caracter == ' ' || Character.isDigit(caracter)) {
                                         accesionLongitud += caracter;
                                   }
                            }
                            if (i == accesion.length() && accesionLongitud.length() > 0) {
                                   if (accesionLongitud.length() == 1) {
                                         accesion = "G    " + accesion;
                                   } else if (accesionLongitud.length() == 2) {   
                                        accesion = "G   " + accesion;
                                   } else if (accesionLongitud.length() == 3) {
                                         accesion = "G  " + accesion;
                                   } else if (accesionLongitud.length() == 4) {
                                         accesion = "G " + accesion;
                                   } else {
                                         accesion = "G" + accesion;
                                   }
                            }
                     }else{//Pastos
                            accesion = accesion.trim().toUpperCase();
                            String accesionLongitud = "";
                            char caracter;
                            int i;
                            for (i = 0; i < accesion.length(); i++) {
                                   caracter = accesion.charAt(i);
                                   if (caracter != ' ' && !Character.isLetterOrDigit(caracter)) {
                                         break;
                                   }
                                   if (caracter == ' ' || Character.isDigit(caracter)) {
                                         accesionLongitud += caracter;
                                   }
                            }
                            if (i == accesion.length() && accesionLongitud.length() > 0) {
                                   if (accesionLongitud.length() == 1) {
                                         accesion = "    " + accesion;
                                   } else if (accesionLongitud.length() == 2) {
                                         accesion = "   " + accesion;
                                   } else if (accesionLongitud.length() == 3) {
                                         accesion = "  " + accesion;
                                   } else if (accesionLongitud.length() == 4) {
                                         accesion = " " + accesion;
                                   }
                            }
                     }
             }
             return accesion;
       }

	
}
