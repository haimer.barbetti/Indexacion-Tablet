package org.cgiar.ciat.indexacion_tablet;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class FragmentELISA extends Fragment{
   @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
      View view=inflater.inflate(R.layout.fragment_elisa, container, false); 
	  return view;
   }
}
