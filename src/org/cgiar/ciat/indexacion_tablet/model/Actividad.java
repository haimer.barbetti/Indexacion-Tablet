package org.cgiar.ciat.indexacion_tablet.model;

import java.util.ArrayList;

public class Actividad {
	
	private String nombre;
	private Estado estado;
	
	public Actividad() {
		super();
	}
	
	public Actividad(String nombre, Estado estado) {
		super();
		this.nombre = nombre;
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Actividad [nombre=" + nombre + ", estado=" + estado + "]";
	}

	

	
	
	
}
