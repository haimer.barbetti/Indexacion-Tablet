package org.cgiar.ciat.indexacion_tablet.model;

import java.io.Serializable;

public class Usuario implements Serializable{
	private String nombres;
	private String nombre_usuario;
	private String contrasenia;
	private int position;
	
	
	public Usuario() {
	}

	public Usuario(String nombres, String nombre_usuario, String contrasenia) {
		super();
		this.nombres = nombres;
		this.nombre_usuario = nombre_usuario;
		this.contrasenia = contrasenia;
	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getNombre_usuario() {
		return nombre_usuario;
	}
	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
	/*@Override
	public String toString() {
		return "Usuario [nombres=" + nombres + ", nombre_usuario="
				+ nombre_usuario + ", contrasenia=" + contrasenia + "]";
	}*/
	
	
	
}
