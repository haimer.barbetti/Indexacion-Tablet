package org.cgiar.ciat.indexacion_tablet.model;

public class Virus {

	private String nombre;
	private Boolean evaluado;

	public Virus() {
		super();
	}

	public Virus(String nombre, Boolean evaluado) {
		super();
		this.nombre = nombre;
		this.evaluado = evaluado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getEvaluado() {
		return evaluado;
	}

	public void setEvaluado(Boolean evaluado) {
		this.evaluado = evaluado;
	}

	@Override
	public String toString() {
		return "Virus [nombre=" + nombre + ", evaluado=" + evaluado + "]";
	}

	

	
}
