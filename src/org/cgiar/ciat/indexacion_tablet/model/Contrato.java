package org.cgiar.ciat.indexacion_tablet.model;

import java.util.ArrayList;

public class Contrato {

	private String numero;
	private ArrayList<Accesion> accesiones;
	private String responsable;

	public Contrato() {
		super();
	}

	public Contrato(String numero, ArrayList<Accesion> accesiones) {
		super();
		this.numero = numero;
		this.accesiones = accesiones;
	}
	
	public Contrato(String numero, ArrayList<Accesion> accesiones, String responsable) {
		super();
		this.numero = numero;
		this.accesiones = accesiones;
		this.responsable = responsable;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public ArrayList<?> getAccesiones() {
		return accesiones;
	}

	public void setAccesiones(ArrayList<Accesion> accesiones) {
		this.accesiones = accesiones;
	}
	
	
	
	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	@Override
	public String toString() {
		return "Contrato [numero=" + numero + ", accesiones=" + accesiones+ ", responsable=" + responsable + "]";
	}

	
	
	

}
