package org.cgiar.ciat.indexacion_tablet.model;

public class Estado {

	private String nombreNegativo;
	private String nombrePositivo;
	private Boolean estado;

	public Estado() {
		super();
	}

	public Estado(String nombreNegativo, String nombrePositivo, Boolean estado) {
		super();
		this.nombreNegativo = nombreNegativo;
		this.nombrePositivo = nombrePositivo;
		this.estado = estado;
	}

	public String getNombreNegativo() {
		return nombreNegativo;
	}

	public void setNombreNegativo(String nombreNegativo) {
		this.nombreNegativo = nombreNegativo;
	}

	public String getNombrePositivo() {
		return nombrePositivo;
	}

	public void setNombrePositivo(String nombrePositivo) {
		this.nombrePositivo = nombrePositivo;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getStrEstado(){
		if(this.estado!=false)
		  return this.nombreNegativo;
		else
		  return this.nombrePositivo;
		  
	}

	@Override
	public String toString() {
		return "Estado [nombreNegativo=" + nombreNegativo + ", nombrePositivo="+ nombrePositivo + ", estado=" + estado + "]";
	}

	
	

}
