package org.cgiar.ciat.indexacion_tablet.model;

import java.util.ArrayList;

public class MetodoPcr {
	
	private ArrayList<Contrato> contratos;
	
	public MetodoPcr() {
		super();
		this.contratos = new ArrayList<Contrato>();
	}
	
	public MetodoPcr(ArrayList<Contrato> contratos) {
		super();
		this.contratos = contratos;
	}

	public ArrayList<?> getContratos() {
		return contratos;
	}

	public void setContratos(ArrayList<Contrato> contratos) {
		this.contratos = contratos;
	}

	@Override
	public String toString() {
		return "MetodoPcr [contratos=" + contratos + "]";
	}
	
	
}
