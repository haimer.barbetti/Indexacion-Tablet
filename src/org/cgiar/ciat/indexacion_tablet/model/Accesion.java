package org.cgiar.ciat.indexacion_tablet.model;

import java.util.ArrayList;

public class Accesion {
	
	private int id;
	private String numero;
	private ArrayList<Actividad> actividades;
	//private ArrayList<Virus> virus;
	private String virus;
	private String fecha;

	public Accesion() {
		super();
		this.actividades = new ArrayList<Actividad>();
	}

	public Accesion(String numero) {
		super();
		this.numero = numero;
		this.actividades = new ArrayList<Actividad>();
	}

	public Accesion(int id,String numero, ArrayList<Actividad> actividades,String virus, String fecha) {
		super();
		this.id=id;
		this.numero = numero;
		this.actividades = new ArrayList<Actividad>();
		this.actividades = actividades;
		this.virus = virus;
		this.fecha = fecha;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public ArrayList<?> getActividades() {
		return actividades;
	}

	public void setActividades(ArrayList<Actividad> actividades) {
		this.actividades = actividades;
	}

	
	
	
	
	public String getVirus() {
		return virus;
	}

	public void setVirus(String virus) {
		this.virus = virus;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	public Actividad getActividad(){
		Actividad actividad=new Actividad();
		if(this.actividades==null){
			return actividad;
		}else{
			
		  for(int i=0;i<this.actividades.size();i++){
			  
		  }
		  
		}
		
		return actividad;
		
	}
	
	

}
