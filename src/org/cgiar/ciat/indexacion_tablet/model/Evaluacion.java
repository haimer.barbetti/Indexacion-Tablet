package org.cgiar.ciat.indexacion_tablet.model;

import java.util.ArrayList;

public class Evaluacion {
	
	
	private String virus;
	private String tipo;
	private String estado;

	public Evaluacion() {
		
	}
	
	
	public Evaluacion(String virus, String tipo, String estado) {
		super();
		this.virus = virus;
		this.tipo = tipo;
		this.estado = estado;
	}


	public String getVirus() {
		return virus;
	}

	public void setVirus(String virus) {
		this.virus = virus;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}


	@Override
	public String toString() {
		return "Evaluacion [virus=" + virus + ", tipo=" + tipo + ", estado="
				+ estado + "]";
	}


	

	

}
