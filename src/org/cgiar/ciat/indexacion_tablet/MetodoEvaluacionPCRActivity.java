package org.cgiar.ciat.indexacion_tablet;

import java.util.ArrayList;

import org.cgiar.ciat.indexacion_tablet.R.layout;
import org.cgiar.ciat.indexacion_tablet.model.Accesion;
import org.cgiar.ciat.indexacion_tablet.model.Actividad;
import org.cgiar.ciat.indexacion_tablet.model.Estado;
import org.cgiar.ciat.indexacion_tablet.model.Evaluacion;
import org.cgiar.ciat.indexacion_tablet.model.Usuario;
import org.cgiar.ciat.indexacion_tablet.model.Contrato;
import org.cgiar.ciat.indexacion_tablet.model.Virus;
import org.cgiar.ciat.indexacion_tablet.Utility.Validador;




















import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
//import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.os.Build;

public class MetodoEvaluacionPCRActivity extends FragmentActivity{

	public Usuario userActive;
	public Contrato resultado=new Contrato();
	public Accesion accesion=new Accesion();
	ListView  listResult;
	public static Contrato contratoSelected; 
	public static int status;
	
	
	public static String tempVirus="";
	public static 	String tempTipo="";
	public static String tempEstado;
	public static  int optSelected=0;
	
	public static ArrayList<Evaluacion> evaluaciones;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_metodo_evaluacion);
		
		
		
		
		//TextView  filterheaderEstado=(TextView) findViewById(R.id.header_estado_txt_lbl);
		
		final TextView actividadTxt=(TextView)findViewById(R.id.txt_actividad_lbl);
		final Spinner spActividades = (Spinner) findViewById(R.id.sp_actividades);
		final TextView contratoTxt=(TextView)findViewById(R.id.txt_contratos_lbl);
		final Spinner spContratos = (Spinner) findViewById(R.id.sp_contratos);
		final TextView responsableTxtlbl=(TextView) findViewById(R.id.txt_responsable_lbl);
		final TextView responsableTxt=(TextView) findViewById(R.id.txt_responsable);
		//final Button placaBtn=(Button)findViewById(R.id.placa_btn);
		
		
		
		
		//final ListView listResult=(ListView) findViewById(R.id.lview_resultado);
		
		/*filterheaderEstado.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			
				
			}
		});*/
		spActividades.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int posicion, long arg3) {
				
				final AdapterView<?> adapterView=parent;
				final int p=posicion;
				String item = adapterView.getItemAtPosition(p).toString();
				
				
				Fragment fragment = null; 
				
				if(p==0){
					
				}else if(p==1){
					fragment=new FragmentActividadPCRRecepcion(); 
				}else if(p==2){
					fragment=new FragmentActividadPCRExtraccion();
				}else if(p==3){
					fragment=new FragmentActividadPCRSintesis();
				}else if(p==4){
					fragment=new FragmentActividadPCRResultado();
				}else{
					
				}
				
				if(fragment!=null&&p!=0){
					
					   FragmentManager manager=getFragmentManager();
		      		   FragmentTransaction transaction=manager.beginTransaction();
		      		   transaction.replace(R.id.fragment_container, fragment);
		      		   transaction.addToBackStack(null);
		      		   transaction.commit();
		      		   
		      		 
				}
				
				  
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			
			}
			
		});
		
		spContratos.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int posicion, long arg3) {
				
				final AdapterView<?> adapterView=parent;
				final int p=posicion;
				status=posicion;
				String item =( (Contrato)adapterView.getItemAtPosition(p)).getNumero().toString();
				final Contrato c=((Contrato) adapterView.getItemAtPosition(p));
				contratoSelected=((Contrato) adapterView.getItemAtPosition(p));
				Log.d(" Hizo Seleccion de Contrato",item);
				
				ListView l=(ListView) findViewById(R.id.lview_resultado);
				ListView lvEvaluaciones=(ListView) findViewById(R.id.lview_resultado_evaluaciones);
				
				
				
			    
				
				//TextView theaderEstadoFilter=(TextView) findViewById(R.id.header_estado_txt_lbl);	
				//TextView theaderAccesionFilter=(TextView) findViewById(R.id.header_accesion_txt_lbl);	
				ImageButton theaderAccesionFilter=(ImageButton) findViewById(R.id.header_accesion_txt_btn);
				ImageButton theaderEstadoFilter=(ImageButton) findViewById(R.id.header_estado_txt_bnt);
				
				//ImageButton byAcesionFilterBtn=(ImageButton) findViewById(R.id.find_by_accesion_btn);
				
				
				//final ListView lviewEvaluaciones=(ListView) findViewById(R.id.lview_evaluaciones);
				
				
				
			
				
				
				
				
				if(p==0){
					
					Log.d(" Imprime","Posicion 0" );
					if(l!=null){
						l.setAdapter(null);
						//lviewEvaluaciones.setAdapter(null);
						lvEvaluaciones.setAdapter(null);
					}
					
					
				}else{
					
					
					
					
					/*byAcesionFilterBtn.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							Log.d(" Imprime ","Prueba");
							FragmentManager manager=getFragmentManager();
							EditEstadoCustomDialogFragment dialogFragment=new EditEstadoCustomDialogFragment();
							dialogFragment.show(manager, "");
						}
						
					});*/
					
					theaderEstadoFilter.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							//Log.d(" Imprime ","Prueba");
							FragmentManager manager=getFragmentManager();
							EditEstadoCustomDialogFragment dialogFragment=new EditEstadoCustomDialogFragment("Estado Recepción");
							dialogFragment.show(manager, "");
						}
						
					});
					
					
					theaderAccesionFilter.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							//Log.d(" Imprime ","Prueba");
							FragmentManager manager=getFragmentManager();
							EditAccionFilterCustomDialogFragment dialogFragment=new EditAccionFilterCustomDialogFragment("Buscar Accesion");
							dialogFragment.show(manager, "");
						}
						
					});
					
					
					if(l!=null){
					
						
						
					l.setAdapter(new ResultadosListViewByContratoAdapter(getApplicationContext(),R.layout.lv_item_row_resultados, c) {

						@Override
						public void onInput(final Object input, View view) {
							
							TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
							TextView procedenciaTxt = (TextView) view.findViewById(R.id.tbl_resultado_procedencia);
							TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
							final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
							final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);
							
							
							TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
							
							idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
							procedenciaTxt.setText("Invitro ");
							accesionTxt.setText(   ((Accesion) input).getNumero() );
							descripcionTxt.setText("");
							ublicacionTxt.setText("Nevera 001");
							
							
							descripcionTxt.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									
									descripcionTxt.setEnabled(true);
									descripcionTxt.requestFocus();
									
								}
							});
							
							estadoTxt.setOnClickListener(new OnClickListener() {

											@Override
											public void onClick(View view) {

												int indexActual = 0;
												ArrayList<String> listEstados = new ArrayList<String>();

												listEstados.add("Pendiente");
												listEstados.add("No Recibido");
												listEstados.add("Recibido");

												if (estadoTxt.getText().toString().equals("Pendiente")) {
													estadoTxt.setText(listEstados.get(1).toString());
												} else if (estadoTxt.getText().toString().equals("No Recibido")) {
													estadoTxt.setText(listEstados.get(2).toString());
												} else if (estadoTxt.getText().toString().equals("Recibido")) {
													estadoTxt.setText(listEstados.get(0).toString());
												} else {
													estadoTxt.setText(listEstados.get(0).toString());
												}

											}

							});
							
							
						}

					}); 
					
					
					
					}
					
					
					final Button placaBtn=(Button)findViewById(R.id.placa_btn);
					
					//if(lvevaluaciones!=null){
					  if(lvEvaluaciones!=null){
						
						  placaBtn.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									
									FragmentManager manager=getFragmentManager();
									EditAccionFilterCustomDialogFragment dialogFragment=new EditAccionFilterCustomDialogFragment("Buscar Placa");
									dialogFragment.show(manager, "");
								}
								
							});
						  
						//LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
						//View v = inflater.inflate(R.layout.activity_metodo_evaluacion, null);
  
						//LinearLayout ly=(LinearLayout) v;  
						
						//ly.setWeightSum(4);
						
						
						//LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT, 1.0f);
						//YOUR_VIEW.setLayoutParams(params);
						//placaBtn.setVisibility(View.VISIBLE);
						
						//LinearLayout.LayoutParams paramLy = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
						//ly.setLayoutParams(paramLy);
						
						//setWeightSum(4);
						
					//	LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT, 0.4f);
						
						//param.weight=0.4f;
						//ly.setLayoutParams(param);
						
						/*param.weight=0.4f;
						actividadTxt.setLayoutParams(param);
						
						param.weight=0.6f;
						spActividades.setLayoutParams(param);
						
						param.weight=0.35f;
						contratoTxt.setLayoutParams(param);
						
						param.weight=0.6f;
						spContratos.setLayoutParams(param);
						
						param.weight=0.55f;
						responsableTxtlbl.setLayoutParams(param);
						
						param.weight=1.05f;
						responsableTxt.setLayoutParams(param);
						
						param.weight=0.45f;
						placaBtn.setLayoutParams(param);*/
						
						
						
						
						
						
						
						
						
						/*placaBtn.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
								
							}
						});*/
						
						lvEvaluaciones.setAdapter(new ResultadosListViewByContratoAdapter(getApplicationContext(),R.layout.lv_item_row_evaluaciones, c) {

							@Override
							public void onInput(final Object input, View view) {
								
								TextView pozoTxt = (TextView) view.findViewById(R.id.tbl_evaluaciones_pozo);
								TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_evaluaciones_accesion);
								TextView virusTxt = (TextView) view.findViewById(R.id.tbl_evaluaciones_virus);
								final TextView pcrTxt = (TextView) view.findViewById(R.id.tbl_evaluaciones_pcr);
								
								
								Accesion tempAccesion=((Accesion) input);
								
								pozoTxt.setText(String.valueOf( tempAccesion.getId()) );
								accesionTxt.setText( tempAccesion.getNumero() );
								virusTxt.setText(  tempAccesion.getVirus().toString()  );
								pcrTxt.setText("Negativo");
								
								
								
								/*
								TextView idTxt = (TextView) view.findViewById(R.id.tbl_resultado_id);
								TextView procedenciaTxt = (TextView) view.findViewById(R.id.tbl_resultado_procedencia);
								TextView accesionTxt = (TextView) view.findViewById(R.id.tbl_resultado_accesion);
								final EditText descripcionTxt = (EditText) view.findViewById(R.id.tbl_resultado_descripcion);
								final TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado);*/
								
								
								//--TextView ublicacionTxt = (TextView) view.findViewById(R.id.tbl_resultado_ubicacion);
								
								//--idTxt.setText( String.valueOf( ((Accesion) input).getId()) );
								//--procedenciaTxt.setText("Invitro ");
								//--accesionTxt.setText(   ((Accesion) input).getNumero() );
								//--descripcionTxt.setText("");
								//--ublicacionTxt.setText("Nevera 001");
								
								
								/*--descripcionTxt.setOnClickListener(new OnClickListener() {
									
									@Override
									public void onClick(View v) {
										
										descripcionTxt.setEnabled(true);
										descripcionTxt.requestFocus();
										
									}
								});*/
								
								pcrTxt.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View view) {

													int indexActual = 0;
													ArrayList<String> listEstados = new ArrayList<String>();

													listEstados.add("Pendiente");
													listEstados.add("Negativo");
													listEstados.add("Positivo");

													if (pcrTxt.getText().toString().equals("Pendiente")) {
														pcrTxt.setText(listEstados.get(1).toString());
													} else if (pcrTxt.getText().toString().equals("Negativo")) {
														pcrTxt.setText(listEstados.get(2).toString());
													} else if (pcrTxt.getText().toString().equals("Positivo")) {
														pcrTxt.setText(listEstados.get(0).toString());
													} else {
														pcrTxt.setText(listEstados.get(0).toString());
													}

												}

								});
								
								
							}

						}); 
						
						
						
						}
					
					
						/*
					if(lviewEvaluaciones!=null){
						RadioGroup  radiosBtnsTipos=(RadioGroup) findViewById(R.id.input_radioGroup_tipo);
						RadioButton radioOptGelTipo = (RadioButton) radiosBtnsTipos.getChildAt(0);
					    RadioButton radioOptPcrTipo = (RadioButton) radiosBtnsTipos.getChildAt(1);
					    Spinner spVirus=(Spinner)view.findViewById(R.id.sp_virus);
						Spinner spEstados=(Spinner)view.findViewById(R.id.sp_evaluacion_estados);
						Button agregarBtn=(Button) view.findViewById(R.id.agregar_btn);
						
						spVirus.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> parent, View arg1, int posicion,long arg3) {
								
								final AdapterView<?> adapterView=parent;
								final int p=posicion;
								//String item = adapterView.getItemAtPosition(p).toString();
								
								tempVirus=adapterView.getItemAtPosition(p).toString();
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								
								
							}
						});
						   
						   spEstados.setOnItemSelectedListener(new OnItemSelectedListener() {

								@Override
								public void onItemSelected(AdapterView<?> parent, View view, int posicion,long arg3) {
									
									final AdapterView<?> adapterView=parent;
									final int p=posicion;
									tempEstado=adapterView.getItemAtPosition(p).toString();
									
								}

								@Override
								public void onNothingSelected(AdapterView<?> arg0) {
									
									
								}
							});
						   
						   
						   radiosBtnsTipos.setOnCheckedChangeListener(new OnCheckedChangeListener() {
								
								@Override
								public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
									
									if(checkId==R.id.input_radio_tipo_pcr){
										optSelected=1;
									}else if(checkId==R.id.input_radio_tipo_pcr_gel){
										optSelected=2;
									}else{
										optSelected=0;
									}
									
											switch (optSelected) {
											case 0:
												tempTipo="";
												break;
											case 1:
												tempTipo="PCR";
												break;
											case 2:
												tempTipo="GEL";
												break;
											default:
												tempTipo="";
												break;
											}
								}
							});
						   
						   
						   agregarBtn.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
								
								
								//lviewEvaluaciones.setAdapter(null);
								//addEvaluacion(new Evaluacion(tempVirus.toString(),tempTipo.toString(),tempEstado.toString()));
								evaluaciones.add(new Evaluacion(tempVirus.toString(),tempTipo.toString(),tempEstado.toString()));
								
								
								
								
								lviewEvaluaciones.setAdapter(new EvaluacionesListViewAdapter(getApplicationContext(),R.layout.lv_item_row_evaluaciones, evaluaciones) {
									
									@Override
									public void onInput(Object input, View view) {
										
										TextView virusTxt = (TextView) view.findViewById(R.id.tbl_resultado_virus);
										TextView tipoTxt = (TextView) view.findViewById(R.id.tbl_resultado_tipo);
										TextView estadoTxt = (TextView) view.findViewById(R.id.tbl_resultado_estado_eval);
										
										virusTxt.setText( String.valueOf( ((Evaluacion) input).getVirus().toString()) );
										tipoTxt.setText( String.valueOf( ((Evaluacion) input).getTipo().toString()) );
										estadoTxt.setText( String.valueOf( ((Evaluacion) input).getEstado().toString()) );
										
									}
								});
								
								
								
							}
						});
					}*/
				
					
					
					
					
				}
			        
				
				
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
		
		
		userActive = (Usuario) getIntent().getSerializableExtra("userActive");
		
		Log.d("debug ",userActive.getNombre_usuario() + " " + userActive.getPosition());

	
		
		/**
		 * Carga los Contratos en la Lista
		 */
		spContratos.setAdapter(new UsersSpinnerAdapter(getApplicationContext(),R.layout.sp_dropdown_item_row_contrato, MainActivity.listContrato) {
			
			@Override
			public void onInput(Object input, View view) {
				TextView userNameTxt = (TextView) view.findViewById(R.id.numContratoDropDown);
				userNameTxt.setText(((Contrato) input).getNumero());
			}

		});
		
		spActividades.setAdapter(new ActividadesSpinnerAdapter(getApplicationContext(),R.layout.sp_dropdown_item_row_actividad, MainActivity.listActividades) {
			
			@Override
			public void onInput(Object input, View view) {
				
				TextView userNameTxt = (TextView) view.findViewById(R.id.numContratoDropDown);
				userNameTxt.setText(((Actividad) input).getNombre());
			}
		});
		
		responsableTxt.setText(String.valueOf(userActive.getNombres()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_pcr, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_pcr_actividad_recepcion,container, false);
			return rootView;
		}
	}
	
	public Contrato getContratoByAccesion(String accesion){
		Accesion temp=new Accesion();
		Contrato contrato=new Contrato();
		
		for(int i=0;i<MainActivity.listContrato.size();i++){
			
			for(int j=0;j<MainActivity.listContrato.get(i).getAccesiones().size();j++){
				temp=(Accesion) MainActivity.listContrato.get(i).getAccesiones().get(j);
				
				if(temp.getNumero().toString().equals(accesion.toString())){
					contrato=MainActivity.listContrato.get(i);
				}
			}
			
			
		}
		return contrato;
	}
	public Contrato getContratoByContrato(String contrato){
		Accesion temp=new Accesion();
		Contrato contratoResultado=new Contrato();
		
		for(int i=0;i<MainActivity.listContrato.size();i++){
			
			if(contrato.toString().equals(contratoResultado.getNumero().toString())){
				contratoResultado=MainActivity.listContrato.get(i);
			}
			
			/*for(int j=0;j<MainActivity.listContrato.get(i).getAccesiones().size();j++){
				temp=(Accesion) MainActivity.listContrato.get(i).getAccesiones().get(j);
				
				if(temp.getNumero().toString().equals(accesion.toString())){
					contratoResultado=MainActivity.listContrato.get(i);
				}
			}*/
			
			
		}
		return contratoResultado;
	}
	
	
	public Accesion getAccesionByAccessionInContrato(Contrato c,String accesion){
		Accesion temp=new Accesion();
		for(int i=0;i<c.getAccesiones().size();i++){
			
			//temp=(Accesion)c.getAccesiones().get(i);
			if(((Accesion) c.getAccesiones().get(i)).getNumero().toString().equals(accesion.toString())){
				temp=((Accesion) c.getAccesiones().get(i));
			}
			
		}
		return temp;
	}
	/*private void showEditDialog() {
	      FragmentManager fm = getSupportFragmentManager();
	      EditEstadoCustomDialogFragment editCustomDialogFragment = EditEstadoCustomDialogFragment.newInstance("Dialogo de Edicion");
	      editCustomDialogFragment.show(fm, "fragment_edit_name");
	      
	  }*/

	

}
